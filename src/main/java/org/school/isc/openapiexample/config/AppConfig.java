package org.school.isc.openapiexample.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackages = {"org.school.api.isc"}
)
public class AppConfig {
}
