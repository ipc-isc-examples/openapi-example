package org.school.isc.openapiexample.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.school.isc.api.PetsApi;
import org.school.isc.model.Pet;
import org.school.isc.model.Pets;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
public class PetsController implements PetsApi {

    @java.lang.Override
    public Optional<ObjectMapper> getObjectMapper() {
        return PetsApi.super.getObjectMapper();
    }

    @java.lang.Override
    public Optional<HttpServletRequest> getRequest() {
        return PetsApi.super.getRequest();
    }

    @java.lang.Override
    public Optional<String> getAcceptHeader() {
        return PetsApi.super.getAcceptHeader();
    }

    @java.lang.Override
    public ResponseEntity<Void> createPets() {
        return ResponseEntity.ok().build();
    }

    @java.lang.Override
    public ResponseEntity<Pets> listPets(Integer limit) {
        final Pets pets = new Pets();

        final Pet dog = new Pet();
        dog.setId(111L);
        dog.setName("Rex");
        dog.setTag("Smart");
        pets.add(dog);

        final Pet cat = new Pet();
        cat.setId(112L);
        cat.setName("Garfield");
        cat.setTag("Funny");
        pets.add(cat);

        final Pet hamster = new Pet();
        hamster.setId(113L);
        hamster.setName("Homa");
        hamster.setTag("Small");
        pets.add(hamster);

        return ResponseEntity.ok(pets);
    }

    @java.lang.Override
    public ResponseEntity<Pet> showPetById(String petId) {
        final Pet body = new Pet();
        body.setId(Long.valueOf(petId));
        body.setName("Default name");
        body.setTag("Default tag");
        return ResponseEntity.ok(body);
    }
}
